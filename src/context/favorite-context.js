import { createContext, useContext, useEffect, useState } from 'react';

const FavoriteContext = createContext({});

export const FavoriteContextProvider = ({ children }) => {
  const [fav, setFav] = useState([]);

  useEffect(() => {
    localStorage.setItem('chars', JSON.stringify(fav));
  }, [fav]);

  useEffect(() => {
    const chars = localStorage.getItem('chars');
    if (chars) {
      setFav(JSON.parse(chars));
    }
  }, []);

  return (
    <FavoriteContext.Provider value={{ fav, setFav }}>
      {children}
    </FavoriteContext.Provider>
  );
};

export const useFavoriteContext = () => useContext(FavoriteContext);
