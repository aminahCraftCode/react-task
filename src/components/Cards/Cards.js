import React from 'react';
import { Link } from 'react-router-dom';
import { useFavoriteContext } from '../../context/favorite-context';
import './Cards.scss';
import FavoriteButton from './FavoriteButton';

const Cards = ({ results, page }) => {
  const { fav, setFav } = useFavoriteContext();
  console.log(fav);
  const handleFavoriteClick = (char) => {
    //if (!fav.Contains[char])
    setFav((prev) => [...prev, char]);
  };

  let display;
  if (results) {
    display = results.map((x) => {
      let { id, name, image, location, status } = x;

      return (
        <div className="col-4 mb-4 position-relative ">
          <Link to={`${page}${id}`} key={id}>
            <div className="cards">
              <img src={image} alt="" className="img-fluid rounded-top" />
              <div className="content-padding content">
                <div className="fs-4 fw-bold mb-4"> {name} </div>
                <div className="">
                  <div className="fs-6"> Last location</div>
                  <div> {location.name}</div>
                </div>
              </div>
            </div>
            {/*<FavoriteButton
              id={id}
              name={name}
              image={image}
              location={location}
              status={status}
              onClick={() => {
                handleFavoriteClick(x);
              }}
            />*/}
            {(() => {
              if (status === 'Dead') {
                return (
                  <div className="badge position-absolute badge bg-danger">
                    {status}
                  </div>
                );
              } else if (status === 'Alive') {
                return (
                  <div className="badge position-absolute badge bg-success">
                    {status}
                  </div>
                );
              } else {
                return (
                  <div className="badge position-absolute badge bg-secondary">
                    {status}
                  </div>
                );
              }
            })()}
          </Link>
          <FavoriteButton
            id={id}
            name={name}
            image={image}
            location={location}
            status={status}
            onClick={() => {
              handleFavoriteClick(x);
            }}
          />{' '}
        </div>
      );
    });
  } else {
    display = 'No Characters Found :/';
  }
  return <>{display}</>;
};

export default Cards;
