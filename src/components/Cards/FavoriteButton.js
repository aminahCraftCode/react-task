import React from 'react';

const FavoriteButton = ({ onClick }) => {
  return (
    <div onClick={onClick}>
      <div style={{ width: '85%', margin: '1rem auto' }}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
          }}
        >
          <button>Add to Favorite</button>
        </div>
      </div>
    </div>
  );
};

export default FavoriteButton;
