import React, { useEffect, useState } from 'react';
import './Cards.scss';
import { useParams } from 'react-router-dom';

const Details = () => {
  let { id } = useParams();
  let [fetchedData, updatedFetchedData] = useState([]);
  let { name, image, gender, status, type, species } = fetchedData;

  let api = `https://rickandmortyapi.com/api/character/${id}`;

  useEffect(() => {
    (async function () {
      let data = await fetch(api)
        .then((res) => res.json())
        .catch((err) => {
          console.log(err);
        });
      updatedFetchedData(data);
    })();
  }, [api]);

  return (
    <div className="container d-flex justify-content-center">
      <div className="d-flex flex-column gap-3">
        <h1 className="">{name}</h1>
        <img src={image} alt="Girl in a jacket" />
        {(() => {
          if (status === 'Dead') {
            return <div className="badge badge bg-danger">{status}</div>;
          } else if (status === 'Alive') {
            return <div className="badge badge bg-success">{status}</div>;
          } else {
            return <div className="badge badge bg-secondary">{status}</div>;
          }
        })()}

        <div className="content">
          <div className="">
            <span className="fw-bold">Gender: </span> {gender}
          </div>
          <div className="">
            <span className="fw-bold">species: </span> {species}
          </div>
          <div className="">
            <span className="fw-bold">type: </span> {type}
            <div className="">
              <span className="fw-bold">status: </span> {status}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Details;
