import React from 'react';
import './Search.scss';

const Search = ({ setSearch, setPageNumber }) => {
  return (
    <form className="flex-sm-row flex-column align-items-center d-flex justify-content-center gap-4 mb-5">
      <input
        onChange={(e) => {
          setPageNumber(1);
          setSearch(e.target.value);
        }}
        placeholder="Search for characters"
        type="text"
        className="input"
      />
      <button
        onClick={(e) => {
          setSearch(setSearch);
          console.log('GFGhf');
          e.preventDefault();
        }}
        className="btn btn-primary fs-5 btn-shadow"
      >
        {' '}
        Search
      </button>
    </form>
  );
};

export default Search;
