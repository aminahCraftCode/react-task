import React from 'react';
import { NavLink, Link } from 'react-router-dom';

const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-lg bg-light">
      <div className="container">
        <Link to="/" className="fs-3 ubuntu navbar-brand">
          Rick & Morty <span className="text-primary"> WiKi</span>
        </Link>

        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="navbar-nav fs-5">
          <NavLink to="/" className="nav-link active" aria-current="page">
            Characters
          </NavLink>
          <NavLink to="/episode" className="nav-link">
            Episodes
          </NavLink>
          <NavLink to="/location" className="nav-link">
            Locations
          </NavLink>
          <NavLink to="/fav" className="nav-link active" aria-current="page">
            Favorite
          </NavLink>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
