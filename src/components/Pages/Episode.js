import React, { useState, useEffect } from 'react';
import Card from '../Cards/Cards';
import Input from '../Filters/categories/Input';
import { useQuery } from 'react-query';

const Episode = () => {
  let [id, setID] = useState(1);
  let [info, setInfo] = useState([]);
  let [results, setResults] = useState([]);
  let { air_date, name } = info;
  let api = `https://rickandmortyapi.com/api/episode/${id}`;

  /*  useEffect(() => {
    (async function () {
      let data = await fetch(api).then((res) => res.json());
      setInfo(data);

      let a = await Promise.all(
        data.characters.map((x) => {
          return fetch(x).then((res) => res.json());
        }),
      );
      setResults(a);
    })();
  });
*/

  const getData = async () => {
    let data = await fetch(api).then((res) => res.json());
    setInfo(data);

    let a = await Promise.all(
      data.characters.map((x) => {
        return fetch(x).then((res) => res.json());
      }),
    );
    setResults(a);
  };
  const { data, status } = useQuery('cards', getData);

  if (status === 'loading') {
    return <div> here </div>;
  }
  if (status === 'error') {
    return <div> Error </div>;
  }

  return (
    <div className="container">
      <div className="row">
        <h1 className="text-center mb-4">
          Episode :
          <span className="text-primary">{name === '' ? 'UnKnown' : name}</span>
        </h1>
        <h5 className="text-center">
          Air Date {air_date === '' ? 'UnKnown' : air_date}
        </h5>
      </div>
      <div className="row">
        <div className="col-3">
          <h4 className="text-center mb-4">Pick Episode</h4>
          <Input setID={setID} total={51} />
        </div>
        <div className="col-8">
          <div className="row">
            <Card page="/episode/" results={results} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Episode;
