import React, { useState, useEffect } from 'react';
import Card from '../Cards/Cards';
import Input from '../Filters/categories/Input';

const Location = () => {
  let [id, setID] = useState(1);
  let [info, setInfo] = useState([]);
  let [results, setResults] = useState([]);
  let { name, type, dimension } = info;
  let api = `https://rickandmortyapi.com/api/location${id}`;

  useEffect(() => {
    (async function () {
      let data = await fetch(api).then((res) => res.json());
      setInfo(data);
      console.log(data);

      let a = await Promise.all(
        data.location.map((x) => {
          return fetch(x)
            .then((res) => res.json())
            .catch((er) => {
              console.log('NOTICE ' + er);
            });
        }),
      );
      setResults(a);
      console.log('hi');
      console.log(a);
    })();
  });
  return (
    <div className="container">
      <div className="row">
        <h1 className="text-center mb-4">
          Location :
          <span className="text-primary">{name === '' ? 'UnKnown' : name}</span>
        </h1>
        <h5 className="text-center">
          Dimension {dimension === '' ? 'UnKnown' : dimension}
        </h5>
        <h6 className="text-center">Type {type === '' ? 'UnKnown' : type}</h6>
      </div>
      <div className="row">
        <div className="col-3">
          <h4 className="text-center mb-4">Pick Location</h4>
          <Input setID={setID} total={126} />
        </div>
        <div className="col-8">
          <div className="row">
            <Card page="/location/" results={results} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Location;
