import React from 'react';
import Card from '../Cards/Cards';
import { useFavoriteContext } from '../../context/favorite-context';

const Fav = () => {
  const { fav } = useFavoriteContext();

  return (
    <div className="container">
      <div className="row">
        <h1 className="text-center mb-4">Favorite </h1>
      </div>
      {/* <div>{results}</div> */}
      <div className="row">
        <div className="col-12">
          <div className="row">
            <Card page="/episode/" results={fav} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Fav;
