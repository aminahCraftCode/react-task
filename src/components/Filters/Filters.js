import React from 'react';
import Gender from './categories/Gender';
import Species from './categories/Species';
import Status from './categories/Status';

const filters = ({ setSpecies, setGender, setStatus, setPageNumber }) => {
  return (
    <div className="col-3">
      <div className="text-center fw-bold fs-4 mb-4"> Filter </div>
      <div
        style={{ cursor: 'pointer' }}
        className="text-center text-primary text-decoration text-underline mb-4"
      >
        {' '}
        Clear Filters
      </div>

      <div className="accordion" id="accordionExample">
        <Gender setPageNumber={setPageNumber} setGender={setGender} />
        <Species setPageNumber={setPageNumber} setSpecies={setSpecies} />
        <Status setPageNumber={setPageNumber} setStatus={setStatus} />
      </div>
    </div>
  );
};

export default filters;
