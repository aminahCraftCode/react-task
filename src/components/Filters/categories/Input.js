import React from 'react';

const Input = ({ total, setID }) => {
  return (
    <div class="input-group mb-3">
      <select
        onChange={(e) => setID(e.target.value)}
        class="custom-select"
        id="inputGroupSelect01"
      >
        <option selected>Choose...</option>
        {[...Array(total).keys()].map((x) => {
          return <option value={x + 1}>Episode - {x + 1}</option>;
        })}
      </select>
    </div>
  );
};

export default Input;
