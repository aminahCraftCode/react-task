import React, { useState } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap';
import {
  FavoriteContextProvider,
  useFavoriteContext,
} from './context/favorite-context';
import { useQuery } from 'react-query';
import { QueryClientProvider, QueryClient } from 'react-query';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import Filters from './components/Filters/Filters';
import Cards from './components/Cards/Cards';
import Pagination from './components/Pagination/Pagination';
import Search from './components/Search/Search';
import Navbar from './components/Navbar/Navbar';
import Location from './components/Pages/Location';
import Episode from './components/Pages/Episode';
import Details from './components/Cards/Details';
import Fav from './components/Pages/Fav';

const queryClient = new QueryClient();
function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <FavoriteContextProvider>
        <Router>
          <div className="App">
            <Navbar />
          </div>

          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/:id" element={<Details />} />
            <Route path="/location" element={<Location />} />
            <Route path="/location/:id" element={<Details />} />
            <Route path="/episode" element={<Episode />} />
            <Route path="/episode/:id" element={<Details />} />
            <Route path="/fav" element={<Fav></Fav>} />
          </Routes>
        </Router>
      </FavoriteContextProvider>
    </QueryClientProvider>
  );
}

const Home = () => {
  const dddd = useFavoriteContext();
  console.log(dddd);
  let [pageNumber, setPageNumber] = useState(1);
  let [search, setSearch] = useState('');
  let [status1, setStatus] = useState('');
  let [gender, setGender] = useState('');
  let [species, setSpecies] = useState('');

  //  let [fetchedData, updatedFetchedData] = useState([]);

  //let { info, results } = fetchedData;

  let api = `https://rickandmortyapi.com/api/character/?page=${pageNumber}&name=${search}&status=${status1}&gender=${gender}&species=${species}`;

  const getData = async () => {
    const { data } = await axios
      .get(api, {
        params: { status1, page: pageNumber, name: search, gender, species },
      })
      .catch((er) => {
        console.log('I am wrong');
        console.log(er);
      });
    console.log('data');
    console.log(data);
    // updatedFetchedData(data);
    return { data };
  };

  /*useEffect(() => {
    getData();
  }, [api]);
*/
  const { data, status } = useQuery('cards', getData);

  if (status === 'loading') {
    return <div> here </div>;
  }
  if (status === 'error') {
    return <div> Error </div>;
  }

  if (status === 'success') {
    return (
      <div className="App">
        <Search setPageNumber={setPageNumber} setSearch={setSearch}></Search>

        <div className="container">
          <div className="row">
            <Filters
              setSpecies={setSpecies}
              setGender={setGender}
              setStatus={setStatus}
              setPageNumber={setPageNumber}
            />
            ``
            <div className="col-8">
              <div className="row">
                <Cards page="/" results={data.data.results} />
              </div>
            </div>
          </div>
        </div>
        <Pagination pageNumber={pageNumber} setPageNumber={setPageNumber} />
      </div>
    );
  }
};

export default App;
// axios
// context + favorite page + local storage
// react-query
// material ui
